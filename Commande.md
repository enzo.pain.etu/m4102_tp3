|Méthode|URI		    |Action réalisée			|Retour				|
|:------|:--------------    |:----------------------------------|:------------------------------|
|GET	|/commandes	    |récupère l'ensemble des commandes	|200 et un tableau de commande	|
|GET	|/commandes/{id}    |récupère la commande via son id	|200 et la commande		|
|	|		    |					|404 si la commande n'existe pas|
|GET	|/commandes/{id}/lastname|récupère le nom du commandeur de la commande|200 et le nom du commandeur|
|	|		    |					|404 si la commande n'existe pas|
|POST	|/commandes	    |crée une commande                  |201 et la l'URL de la commande crée|
|	|		    |					|406 si il manque des informations|
|	|		    |					|
|DELETE |/commandes/{id}    |supprime une commande		|202 si la commande est supprimée avec succès|
|	|		    |					|404 si la commande avec cet id n'existe pas|			
