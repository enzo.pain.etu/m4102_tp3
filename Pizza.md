|Méthode|URI		    |Action réalisée			|Retour				|
|:------|:--------------    |:----------------------------------|:------------------------------|
|GET	|/pizzas	    |récupère l'ensemble des pizzas	|200 et un tableau de pizza	|
|GET	|/pizzas/{id}	    |récupère la pizza d'id ID		|200 et la pizza		|
|	|		    |					|404 si la pizza n'existe pas   |
|GET	|/pizzas/{id}/name  |récupère le nom de la pizza avec id|200 et le nom de la pizza	|
|	|		    |					|404 si la pizza n'existe pas	|
|POST	|/pizzas	    |crée une pizza                     |201 et la l'URL de la pizza crée|
|	|		    |					|406 si il manque des informations|
|	|		    |					|409 si une pizza avec ce nom existe déjà|
|	|		    |					|
|DELETE |/pizzas/{id}	    |supprime une pizza			|202 si la pizza est supprimée avec succès|
|	|		    |					|404 si la pizza avec cet id n'existe pas|			
