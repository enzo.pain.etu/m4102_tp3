package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface javax.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class CommandeResourceTest extends JerseyTest {
    private CommandeDao dao;
    
    @Override
    protected Application configure() {
       return new ApiV1();
    }

    // Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la base de données
    // et les DAO
    
    // https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
    @Before
    public void setEnvUp() {
    	dao = BDDFactory.buildDao(CommandeDao.class);
    	dao.createTableAndAss();
    }

    @After
    public void tearEnvDown() throws Exception {
    	dao.dropTable();
    }

    @Test
    public void testGetEmptyList() {
	// La méthode target() permet de préparer une requête sur une URI.
	// La classe Response permet de traiter la réponse HTTP reçue.
        Response response = target("/commandes").request().get();

	// On vérifie le code de la réponse (200 = OK)
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		  
	// On vérifie la valeur retournée (liste vide)
	// L'entité (readEntity() correspond au corps de la réponse HTTP.
	// La classe javax.ws.rs.core.GenericType<T> permet de définir le type
	// de la réponse lue quand on a un type complexe (typiquement une liste).
        List<CommandeDto> Commandes;
        Commandes = response.readEntity(new GenericType<List<CommandeDto>>(){});

        assertEquals(0, Commandes.size());

    }
    
    @Test
    public void testGetNotExistingCommande() {
		Response response = target("/commandes/125").request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }
    
    @Test
    public void testGetExistingCommande() {
		Commande c = new Commande();
		
		c.setNom("Pain");
		c.setPrenom("Anzo");
		c.setPizzas(new ArrayList<Pizza>());
		
		long id = dao.insert(c.getNom(),c.getPrenom(),c.getPizzas());
		c.setId(id);

		Response response = target("/commandes/" + id).request().get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		Commande result  = Commande.fromDto(response.readEntity(CommandeDto.class));
		assertEquals(c, result);
   }

   @Test
   public void testCreateCommande() {
		CommandeCreateDto dto = new CommandeCreateDto();
		dto.setNom("Pain");
		dto.setPrenom("Anzo");
		dto.setPizzas(new ArrayList<Pizza>());
		
		Response response = target("/commandes")
							.request()
							.post(Entity.json(dto));
		
		// On vérifie le code de status à 201
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		
		CommandeDto returnedEntity = response.readEntity(CommandeDto.class);
		
		// On vérifie que le champ d'entête Location correspond à
		// l'URI de la nouvelle entité
		assertEquals(target("/commandes/" +
		returnedEntity.getId()).getUri(), response.getLocation());
				
		assertEquals(returnedEntity.getNom(), dto.getNom());
		assertEquals(returnedEntity.getPizzas(), dto.getPizzas());
   }
   
   @Test
   public void testCreateSameCommande() {
	   CommandeCreateDto dto = new CommandeCreateDto();
		dto.setNom("Pain");
		dto.setPrenom("Anzo");
		dto.setPizzas(new ArrayList<Pizza>());
       
       dao.insert(dto.getNom(), dto.getPrenom(), dto.getPizzas());
       
       Entity<CommandeCreateDto> e = Entity.json(dto);
       
       Response response = target("/commandes")
               .request()
               .post(e);

       assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
   }

   @Test
   public void testCreateCommandeWithoutName() {
      CommandeCreateDto ingredientCreateDto = new CommandeCreateDto();
       
       Response response = target("/commandes")
               .request()
               .post(Entity.json(ingredientCreateDto));

       assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
   }

   @Test
   public void testDeleteExistingCommande() {
	   Commande c = new Commande();
		
		c.setNom("Pain");
		c.setPrenom("Anzo");
		c.setPizzas(new ArrayList<Pizza>());
		
		long id = dao.insert(c.getNom(),c.getPrenom(),c.getPizzas());
		System.out.println(id);
	    c.setId(id);

	    Response response = target("/commandes/" + id).request().delete();

	    assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

	    Commande result = dao.findCommande(id);
		assertEquals(result, null);
   }
   
   @Test
   public void testDeleteNotExistingCommande() {
	   Response response = target("/commandes/125").request().delete();
	   assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
   }

   @Test
   public void testGetCommandeName() {
	   Commande c = new Commande();
		
		c.setNom("Pain");
		c.setPrenom("Anzo");
		c.setPizzas(new ArrayList<Pizza>());
		
		long id = dao.insert(c.getNom(),c.getPrenom(),c.getPizzas());

       Response response = target("/commandes/" + id + "/lastname").request().get();

       assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

       assertEquals("Pain", response.readEntity(String.class));
   }
   
   @Test
   public void testGetNotExistingCommandeName() {
       Response response = target("/commandes/125/name").request().get();

       assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
   }
}
