package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface javax.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class PizzaResourceTest extends JerseyTest {
    private PizzaDao dao;
    
    @Override
    protected Application configure() {
       return new ApiV1();
    }

    // Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la base de données
    // et les DAO
    
    // https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
    @Before
    public void setEnvUp() {
    	dao = BDDFactory.buildDao(PizzaDao.class);
    	dao.createTableAndIngredientAssociation();
    }

    @After
    public void tearEnvDown() throws Exception {
    	dao.dropTable();
    }

    @Test
    public void testGetEmptyList() {
	// La méthode target() permet de préparer une requête sur une URI.
	// La classe Response permet de traiter la réponse HTTP reçue.
        Response response = target("/pizzas").request().get();

	// On vérifie le code de la réponse (200 = OK)
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		  
	// On vérifie la valeur retournée (liste vide)
	// L'entité (readEntity() correspond au corps de la réponse HTTP.
	// La classe javax.ws.rs.core.GenericType<T> permet de définir le type
	// de la réponse lue quand on a un type complexe (typiquement une liste).
        List<PizzaDto> pizzas;
        pizzas = response.readEntity(new GenericType<List<PizzaDto>>(){});

        assertEquals(0, pizzas.size());

    }
    
    @Test
    public void testGetNotExistingPizza() {
		Response response = target("/pizzas/125").request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }
    
    @Test
    public void testGetExistingPizza() {
		Pizza p = new Pizza();
		
		p.setName("Fun");
		p.setIngredients(new ArrayList<Ingredient>());
		
		long id = dao.insert(p.getName(),p.getIngredients());
		p.setId(id);

		Response response = target("/pizzas/" + id).request().get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		Pizza result  = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(p, result);
   }

   @Test
   public void testCreatePizza() {
		PizzaCreateDto dto = new PizzaCreateDto();
		dto.setName("TestDuFun");
		dto.setIngredients(new ArrayList<Ingredient>());
		
		Response response = target("/pizzas")
							.request()
							.post(Entity.json(dto));
		
		// On vérifie le code de status à 201
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		
		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);
		
		// On vérifie que le champ d'entête Location correspond à
		// l'URI de la nouvelle entité
		assertEquals(target("/pizzas/" +
		returnedEntity.getId()).getUri(), response.getLocation());
				
		assertEquals(returnedEntity.getName(), dto.getName());
		assertEquals(returnedEntity.getIngredients(), new ArrayList<Ingredient>());
   }
   
   @Test
   public void testCreateSamePizza() {
	   PizzaCreateDto dto = new PizzaCreateDto();
	   dto.setName("TestDuFun");
	   dto.setIngredients(new ArrayList<Ingredient>());
       
       dao.insert(dto.getName(),dto.getIngredients());

       
       Entity<PizzaCreateDto> e = Entity.json(dto);

       Response response = target("/pizzas")
               .request()
               .post(e);

       assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
   }

   @Test
   public void testCreatePizzaWithoutName() {
      PizzaCreateDto ingredientCreateDto = new PizzaCreateDto();
       
       Response response = target("/pizzas")
               .request()
               .post(Entity.json(ingredientCreateDto));

       assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
   }

   @Test
   public void testDeleteExistingPizza() {
	   Pizza p = new Pizza();
	   
	   p.setName("cEuNEpIZZA");
	   p.setIngredients(new ArrayList<Ingredient>());
	   
	    
	    long id = dao.insert(p.getName(),p.getIngredients());
	    p.setId(id);

	    Response response = target("/pizzas/" + id).request().delete();

	    assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

	    Pizza result = dao.findById(id);
		assertEquals(result, null);
   }
   
   @Test
   public void testDeleteNotExistingPizza() {
	   Response response = target("/pizzas/125").request().delete();
	   assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
   }

   @Test
   public void testGetPizzaName() {
	   Pizza p = new Pizza();
	   
	   p.setName("cEuNEpIZZA");
	   p.setIngredients(new ArrayList<Ingredient>());
	   
       long id = dao.insert(p.getName(),p.getIngredients());

       Response response = target("pizzas/" + id + "/name").request().get();

       assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

       assertEquals("cEuNEpIZZA", response.readEntity(String.class));
   }
   
   @Test
   public void testGetNotExistingPizzaName() {
       Response response = target("pizzas/125/name").request().get();

       assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
   }
}
