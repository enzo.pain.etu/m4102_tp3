package fr.ulille.iut.pizzaland.dto;

public class IngredientCreateDto {
	private String name;
	
	public IngredientCreateDto() {}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
