package fr.ulille.iut.pizzaland.beans;

import java.util.List;

import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

public class Commande {
	private long id;
	private String nom;
	private String prenom;
	private List<Pizza> pizzas;
	
	public Commande() {}
	
	public Commande(long id, String nom, String prenom, List<Pizza> pizzas) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.pizzas = pizzas;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public List<Pizza> getPizzas() {
		return pizzas;
	}
	
	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public static Commande fromDto(CommandeDto dto) {
		Commande c = new Commande();
		c.setNom(dto.getNom());
		c.setPrenom(dto.getPrenom());
		c.setId(dto.getId());
		c.setPizzas(dto.getPizzas());
		
		return c;
	}
	
	public static CommandeDto toDto(Commande c) {
		CommandeDto dto = new CommandeDto();
		dto.setNom(c.getNom());
		dto.setPrenom(c.getPrenom());
		dto.setId(c.getId());
		dto.setPizzas(c.getPizzas());
		
		return dto;
	}

	public static Commande fromCreateDto(CommandeCreateDto dto) {
		Commande c = new Commande();
		
		c.setNom(dto.getNom());
		c.setPrenom(dto.getPrenom());
		c.setPizzas(dto.getPizzas());
		
		return c;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((pizzas == null) ? 0 : pizzas.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Commande other = (Commande) obj;
		if (id != other.id)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (pizzas == null) {
			if (other.pizzas != null)
				return false;
		} else if (!pizzas.equals(other.pizzas))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		return true;
	}
}