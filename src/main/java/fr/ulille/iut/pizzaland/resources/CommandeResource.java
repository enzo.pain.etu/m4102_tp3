package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

@Path("/commandes")
public class CommandeResource {
	private CommandeDao commandes;

	@Context
    public UriInfo uriInfo;

	public CommandeResource() {
		commandes = BDDFactory.buildDao(CommandeDao.class);
		commandes.createTableAndAss();
	}
	
	@GET
	public List<CommandeDto> getAll(){
		 return commandes.getAll().stream().map(Commande::toDto).collect(Collectors.toList());
	}
	
	@GET
    @Path("{id}")
    public CommandeDto getOneCommande (@PathParam("id") long id) {
    	try {
    		Commande c = commandes.findCommande(id);
    		return Commande.toDto(c);
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}
    }
	
	@GET
    @Path("{id}/lastname")
    public String getOneCommandeLastname (@PathParam("id") long id) {
    	try {
    		Commande c = commandes.findCommande(id);
    		return c.getNom();
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}
    }
	
	@POST
    public Response createCommande(CommandeCreateDto dto) {
    	Commande existing = commandes.findByName(dto.getNom(), dto.getPrenom());
    	
        if ( existing != null ) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }
        
        try {
            Commande com = Commande.fromCreateDto(dto);
            long id = commandes.insert(com.getNom(),com.getPrenom(),com.getPizzas());
            com.setId(id);
            
            CommandeDto pDto = Commande.toDto(com);

            URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

            return Response.created(uri).entity(pDto).build();
        }
        catch ( Exception e ) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
	}
	
	@DELETE
    @Path("{id}")
    public Response deletePizza(@PathParam("id") long id) {
    	if ( commandes.findCommande(id) == null ) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}

        commandes.remove(id);

        return Response.status(Response.Status.ACCEPTED).build();
    }
}
